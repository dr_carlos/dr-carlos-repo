#!/usr/bin/env fish

# Exit if we aren't going to build anything
if test ! -e PKGBUILD
   echo "No PKGBUILD!"
   exit 1
end

# Remove previously built packages if they exist
set -l old_pkgs *.pkg.tar.zst{,.sig}
if set -q old_pkgs[1]
    rm $old_pkgs
end

# Get arch-repo dir
set -l repodir (dirname (status --current-filename))

# Build the package, fail on err
$repodir/dr-carlos-repo-x86_64-build; or return

# Find name, version, and release of the built package
set -l pkgname (bash -c 'source PKGBUILD; echo $pkgname')
set -l pkgver (bash -c 'source PKGBUILD; echo $pkgver')
set -l pkgrel (bash -c 'source PKGBUILD; echo $pkgrel')
set -l epoch (bash -c 'source PKGBUILD; echo $epoch')

# Remove all dirs in directory (e.g. git repo) if they exist
set -l dirs */
if set -q dirs[1]
   rm -r --interactive=none $dirs
end

# Generate package name
set -l package "$pkgname-$pkgver-$pkgrel-x86_64.pkg.tar.zst"

if test -n $epoch
    set package "$pkgname-$epoch:$pkgver-$pkgrel-x86_64.pkg.tar.zst"
end

if test ! -e $package
    set package "$pkgname-$pkgver-$pkgrel-any.pkg.tar.zst"

    if test -n $epoch
        set package "$pkgname-$epoch:$pkgver-$pkgrel-any.pkg.tar.zst"
    end
end

# Sign built package, fail on err
gpg --local-user dr_carlos --use-agent --output $package.sig --detach-sig $package; or return

# Copy built package and signature to repo dir, fail on err
cp $package{,.sig} $repodir/x86_64/; or return

# If we're not building the DB
if test (count $argv) -ge 1
    if $argv[1] == "--no-db"
        # Add all the new files to git, fail on err
        git -C $repodir add '.'; or return

        # Commit changes with automatic messsage
        git -C $repodir commit -a -m "(Automatic) Update $pkgname to $(bash -c 'source PKGBUILD; echo $pkgver')-$(bash -c 'source PKGBUILD; echo $pkgrel')"; or return
    end
end

# Re-generate arch-repo db, fail on err
repo-add --key (bash -c 'source $XDG_CONFIG_HOME/pacman/makepkg.conf; echo $GPGKEY') --verify --sign --new --remove $repodir/x86_64/dr-carlos-repo.db.tar.gz $repodir/x86_64/*.pkg.tar.zst; or return

# GitLab can't handle symlinks, so remove them and fail on err
rm $repodir/x86_64/dr-carlos-repo{.db,.db.sig,.files,.files.sig}; or return
mv $repodir/x86_64/dr-carlos-repo.db.tar.gz $repodir/x86_64/dr-carlos-repo.db; or return
mv $repodir/x86_64/dr-carlos-repo.db.tar.gz.sig $repodir/x86_64/dr-carlos-repo.db.sig; or return
mv $repodir/x86_64/dr-carlos-repo.files.tar.gz $repodir/x86_64/dr-carlos-repo.files; or return
mv $repodir/x86_64/dr-carlos-repo.files.tar.gz.sig $repodir/x86_64/dr-carlos-repo.files.sig; or return

# Add all the new files to git, fail on err
git -C $repodir add '.'; or return

# Commit changes with automatic messsage
git -C $repodir commit -a -m "(Automatic) Update $pkgname to $(bash -c 'source PKGBUILD; echo $pkgver')-$(bash -c 'source PKGBUILD; echo $pkgrel')"; or return

# Push changes
git -C $repodir push -u
