#!/usr/bin/env fish

# Sync latest package versions
yay

# Get arch-repo dir
set -l repodir (realpath (dirname (status --current-filename)))

# Update repo and submodules, fail on err
git -C $repodir pull --recurse-submodules; or return
git -C $repodir submodule update --recursive; or return

# Enter packages dir, fail on err
cd $repodir/pkgbuilds; or return

# Build every package
for package in (ls)

	# Enter package dir, fail on err
	cd $package; or return

	# Update sources and thus pkgver(), fail on err
	makepkg --nodeps --noprepare --nobuild --nocheck --noarchive; or return

	# Get package name
	set -l pkgname (bash -c 'source PKGBUILD; echo ${pkgname:-"NO_PKGNAME"}')

	# Get new pkgver, rel, and epoch
	set -l pkgver (bash -c 'source PKGBUILD; echo ${pkgver:-0}')
	set -l pkgrel (bash -c 'source PKGBUILD; echo ${pkgrel:-0}')
	set -l epoch (bash -c 'source PKGBUILD; echo ${epoch:-0}')

    # Get sync version, or move on to next pkg if not installed, fail on err
    set -l sync_version (expac -S "%v" dr-carlos-repo/$pkgname); or echo "Not installed - don't bother rebuilding"; cd .. && continue; or return

	# Compare sync and new local versions
	set -l verdiff (vercmp $sync_version $epoch:$pkgver-$pkgrel)

	# Rebuild if versions are different (higher or lower), fail on err
	if test $verdiff -ne 0
	   fish $repodir/makepkg.fish --no-db; or return
	end

	# Return to pkgbuilds dir, fail on err
	cd ..; or return
end

# Re-generate arch-repo db, fail on err
repo-add --key (bash -c 'source $XDG_CONFIG_HOME/pacman/makepkg.conf; echo $GPGKEY') --verify --sign --new --remove $repodir/x86_64/dr-carlos-repo.db.tar.gz $repodir/x86_64/*.pkg.tar.zst; or return

# GitLab can't handle symlinks, so remove them and fail on err
rm $repodir/x86_64/dr-carlos-repo{.db,.db.sig,.files,.files.sig}; or return
mv $repodir/x86_64/dr-carlos-repo.db.tar.gz $repodir/x86_64/dr-carlos-repo.db; or return
mv $repodir/x86_64/dr-carlos-repo.db.tar.gz.sig $repodir/x86_64/dr-carlos-repo.db.sig; or return
mv $repodir/x86_64/dr-carlos-repo.files.tar.gz $repodir/x86_64/dr-carlos-repo.files; or return
mv $repodir/x86_64/dr-carlos-repo.files.tar.gz.sig $repodir/x86_64/dr-carlos-repo.files.sig; or return

# Add all the new files to git, fail on err
git -C $repodir add '.'; or return

# Commit changes with automatic messsage, fail on err
git -C $repodir commit -a -m "(Automatic) Update db and files"; or return

# Push changes
git -C $repodir push -u
