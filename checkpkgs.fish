#!/usr/bin/env fish

# Check if packages were built against older versions of dependencies
checkrebuild -v -i dr-carlos-repo

# List of submodule packages
set -l submodules (cat .gitmodules | grep submodule | sed 's/\[submodule \"pkgbuilds\///' | sed 's/\"\]//')

# Enter the pkgbuilds dir
cd pkgbuilds

# Check PKGBUILDS that aren't submodule (i.e. ones we can do anything about)
namcap -i (for pkgbuild in (ls); contains $pkgbuild $submodules; if test $status -ne 0; echo "$pkgbuild/PKGBUILD"; end; end)
